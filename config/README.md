# Spring Cloud Config Microservice

## Description
This application uses [Spring Cloud Config](https://cloud.spring.io/spring-cloud-config/) to provide
server and client-side support for externalized configuration in a distributed system. This application
supports file system backend.

## Launching the Microservice
You can launch the Microservice using
```
java  
-Dlogging.root=${logging.root}
-Deureka.client.serviceUrl.defaultZone=${eureka.serviceUrl}
-Dsearch.locations=${search.locations}
-Dencrypt.keyStore.location=${keystore.location}
-Dencrypt.keyStore.password=${keystore.password}
-Dencrypt.keyStore.alias=${keystore.alias}
-jar config-${version}.jar
```

- `${logging.root}` is the root location of the log files
- `${eureka.serviceUrl}` is the Eureka service URL(s)
- `${search.locations}` is the root location of the externalized configuration files
- `${keystore.location}` is the location of the keystore
- `${keystore.password}` is the keystore password
- `${keystore.alias}` is the key alias
- `${version}` is the version of the artifact

Note the `encrypt.*` arguments are optional and are only required when you want to perform encryption and decryption operations on the Cloud Config. 

### Example
```
java
-Dlogging.root=${user.home}/applog
-Deureka.client.serviceUrl.defaultZone=http://localhost:9761/eureka/
-Dsearch.locations=file:///${user.home}/app/configuration/microservice-configuration/externalized
-Dencrypt.keyStore.location=file:///${user.home}/app/configuration/certificates/f5tomsrvcdev01.aerlingus.com.jks
-Dencrypt.keyStore.password=[removed for security]
-Dencrypt.keyStore.alias=1
-jar config-1.0.0-GDM-SNAPSHOT.jar
```

## Validate Application Deployment
Search for the string `Tomcat started on port(s):` in the log file `${logging.root}/config/config.log`

## Encryption and Decryption
Asymmetric key (RSA key pair) shall be used for [Encryption and Decryption](http://cloud.spring.io/spring-cloud-static/spring-cloud-config/2.0.1.RELEASE/single/spring-cloud-config.html#_encryption_and_decryption). 

### Encryption
```
curl http://localhost:9090/config/encrypt --data-urlencode ${sensitiveInformation}
```
- `${sensitiveInformation}` is the sensitive information to be encrypted

### Decryption
```
curl http://localhost:9090/config/decrypt --data-urlencode ${encryptedInformation}
```
- `${encryptedInformation}` is the encrypted information to be decrypted


The config service will automatically decrypt encrypted content as long as `{cipher}` is present at the start of the value
```
mongodb:
  uri:
    dev: '{cipher}AQAwPLQEZwBdAPU1SsBMZN6ZZR5raD1uPUstheQzgvlMASKrDC4u277pZOF/W8SNXqyMAGp13CnRNjtD6jRhL4Jioi6uE9vgLOR/3ppCfYtCum2qX8AyC8ZEoVpYM0yxvqHBOLOhUO8t8hRcWb+l4p5dGqarKu60Rcd4QIC0dCirWvIxFasHxTM32iP+ONEP/2/zOin80nwcpB6+Cc3K+zssBc6mh58QB3mxaG9o2Ie+abL5f1tvjES1NkmEms9T/Hy125+3qMJAs30DRnoCRmeKkTitOdMC5JithiifU3V9JZfH+4w2NKEYJULxpsstW9Hb6fnoEmO7U810iytxayHFFTcbPvwayUp0zpqcXWLnliERnAbSRok6qW6pYMx/2UYfSDLret/jYHKwrnFDPqGeVMGtHd9ORMmDdQmvTk8nn+SeOk9cjsnHporY3x6fE8nx4P3siGD4K/2chOKZLdm127nGWWrmDLUnwnoftz9le1dPpa7pGBGmr1Qehw800UqV/pSgqgqXSL0CKFwwTiKsDQ0DmVJrOgVzKcGi6r4d7ITOLk7O0Ar4tudICBV/xemiJg2U8miwAiHIQLgJciinbNETRewZ8b7/URobmuGUug=='
```

## Good to know
- Depends on `eureka` Microservice to be online
- Log files can be located in `${logging.root}/config`