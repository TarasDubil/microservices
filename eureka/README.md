# Eureka Microservice

## Description
This Microservice is built using [Spring Cloud Netflix Eureka](https://cloud.spring.io/spring-cloud-netflix/) and provides Service Discovery

## Building the Microservice
Run the following maven command
- `mvn clean install`


## Launching the Microservice
You can launch the Microservice using
```
java 
-Dlogging.root=${logging.root}
-Deureka.client.serviceUrl.defaultZone=${eureka.serviceUrl}
-jar eureka-${version}.jar
```

- `${logging.root}` is the root location of the log files
- `${eureka.serviceUrl}` is the Eureka service URL(s)
- `${version}` is the version of the artifact

### Example
```
java
-Dlogging.root=file:///${user.home}/applog
-Deureka.client.serviceUrl.defaultZone=http://localhost:9761/eureka/
-jar eureka-1.0.0-GDM-SNAPSHOT.jar
```

## Validate Application Deployment
Search for the string `Tomcat started on port(s):` in the log file `${logging.root}/eureka/eureka.log`

## Enable Peer Awareness
Eureka can be made more resilient and available by running [multiple instances](http://cloud.spring.io/spring-cloud-netflix/single/spring-cloud-netflix.html#spring-cloud-eureka-server-zones-and-regions).
Eureka clients can then update the `defaultZone` to list all Eureka service URLs, by updating the JVM `eureka.client.serviceUrl.defaultZone` argument
```
-Deureka.client.serviceUrl.defaultZone=http://hostA:9761/eureka/,http://hostB:9761/eureka/
```

## Good to know
- The Eureka web interface can be accessed from `http://localhost:9761`
- Log files can be located in `${logging.root}/eureka`
